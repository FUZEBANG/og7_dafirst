package Primzahlen;

public class Stoppuhr {
	
	private long nanos;
	private double millis;
	

	public void starte() {
		nanos = System.nanoTime();
		millis = nanos / 1000000000.0;
	}

	public void stoppe() {
		nanos = System.nanoTime() - nanos;
		millis = nanos / 1000000000.0;
	}

	public double lies() {
		return millis;
	}

	public long lies1() {
		return nanos;
	}
}
