package Primzahlen;

import java.util.Scanner;

public class Primzahl {
	
	static Stoppuhr uhr = new Stoppuhr();
	static Scanner scanner = new Scanner(System.in);
	static long x;
	
	public static boolean istPrim(long x) {
		if (x <= 1) {
			return false; 
		}
		for (int i = 2; i < x; i++) {
			if (x % i == 0) 
				return false; 
		}
		return true; 
	}
	public static void main(String[] args) {
		System.out.println("Bitte eine Primzahl eingeben:");
		x = scanner.nextLong();
		uhr.starte();
			if (istPrim(x)) {
			System.out.println("Ist eine Primzahl");
			uhr.stoppe();
			System.out.println(uhr.lies() + " Millisekunden");
			System.out.println(uhr.lies1() + " Nanosekunden");
		} else {
			System.out.println("Ist keine Primzahl");
			uhr.stoppe();
			System.out.println(uhr.lies() + " Millisekunden");
			System.out.println(uhr.lies1() + " Nanosekunden");
		}
	}
}
