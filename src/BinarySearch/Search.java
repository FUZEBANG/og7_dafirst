package BinarySearch;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Search {
	
	static int[] list = new int[20000000];
	static int x;
	static Stoppuhr uhr = new Stoppuhr();
	static Random rand = new Random();
	static Scanner scanner = new Scanner(System.in);
	
	public static int fuellen() {
		for (int i = 0; i < list.length; i++) {
			int n = rand.nextInt(15000001);
			list[i] = n;
		}
		return 0;
	}
	
	public static boolean lineareSuche() {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == x) {
				System.out.println(x + " gefunden an der Stelle = " + i);
				return true;
			}
		}
		return false;
	}
	
	public static int binarySearch() {
		System.out.println(x + " gefunden an der Stelle = "+ Arrays.binarySearch(list, x));
		return 0;
	}

	public static void stalinSearch() {
		for (int i = 0; i < list.length; i++) {
			if (list[i] == x) {

			} else if(list[i] != x) {
				list[i] = 0;
			}
		}
	}
	
	public static void main(String[] args) {
		char operator;
		
		System.out.println("Auswahl:");
	    System.out.println(" (L)ineare Suche");
	    System.out.println(" (B)inaere Suche");
	    System.out.println(" (S)talin Suche");
	    System.out.println(" (E)xit ");
	    do {
	    fuellen();
	    operator = scanner.next().charAt(0);
		switch (operator) {
			case 'L': ;
			case 'l':
				System.out.println("Bitte die gesuchte Zahl eingeben: ");
				x = scanner.nextInt();
				uhr.starte();
				lineareSuche();
				uhr.stoppe();
				System.out.println(uhr.liesMillis() + " Millisekunden");
				System.out.println(uhr.liesNanos() + " Nanosekunden");
				uhr.reset();
				System.out.println("Auswahl:");
			    System.out.println(" (L)ineare Suche");
			    System.out.println(" (B)inaere Suche");
			    System.out.println(" (E)xit ");
				break;
			case 'B': ;
			case 'b':
				System.out.println("Bitte die gesuchte Zahl eingeben: ");
				x = scanner.nextInt();
				uhr.starte();
				binarySearch();
				uhr.stoppe();
				System.out.println(uhr.liesMillis() + " Millisekunden");
				System.out.println(uhr.liesNanos() + " Nanosekunden");
				uhr.reset();
				System.out.println("Auswahl:");
			    System.out.println(" (L)ineare Suche");
			    System.out.println(" (B)inaere Suche");
			    System.out.println(" (E)xit ");
				break;
			case 'S': ;
			case 's':
				System.out.println("Bitte Stalin-Zahl eingeben: ");
				 x = scanner.nextInt();
				 uhr.starte();
				 stalinSearch();
				break;
			case 'E': ;
			case 'e':
				 System.exit(0);
				break;
		}
	} while (true);
	}
}
