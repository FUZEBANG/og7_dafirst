package BinarySearch;

public class Stoppuhr {
	
	private long nanos;
	private double millis;
	

	public void starte() {
		nanos = System.nanoTime();
		millis = nanos / 1000000000.0;
	}

	public void stoppe() {
		nanos = System.nanoTime() - nanos;
		millis = nanos / 1000000000.0;
	}

	public double liesMillis() {
		return millis;
	}

	public long liesNanos() {
		return nanos;
	}
	
	public void reset() {
		nanos = 0;
		millis = 0;
	}
}
