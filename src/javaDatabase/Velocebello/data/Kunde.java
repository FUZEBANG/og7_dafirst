package javaDatabase.Velocebello.data;

public class Kunde {
	
	//Attribute
	private int kundennummer;
	private String vorname;
	private String nachname;
	private String strasse;
	private String plz;
	private String ort;
	private String telefonnummer;
	
	//Konstruktor
	public Kunde(int kundennummer, String vorname, String nachname, String strasse, String plz, String ort,
			String telefonnummer) {
		super();
		this.kundennummer = kundennummer;
		this.vorname = vorname;
		this.nachname = nachname;
		this.strasse = strasse;
		this.plz = plz;
		this.ort = ort;
		this.telefonnummer = telefonnummer;
	}

	//Methoden
	
	public int getKundennummer() {
		return kundennummer;
	}

	public void setKundennummer(int kundennummer) {
		this.kundennummer = kundennummer;
	}

	public String getVorname() {
		return vorname;
	}

	public void setVorname(String vorname) {
		this.vorname = vorname;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getStrasse() {
		return strasse;
	}

	public void setStrasse(String strasse) {
		this.strasse = strasse;
	}

	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getTelefonnummer() {
		return telefonnummer;
	}

	public void setTelefonnummer(String telefonnummer) {
		this.telefonnummer = telefonnummer;
	}

	@Override
	public String toString() {
		return "Kunde [kundennummer=" + kundennummer + ", vorname=" + vorname + ", nachname=" + nachname + ", strasse="
				+ strasse + ", plz=" + plz + ", ort=" + ort + ", telefonnummer=" + telefonnummer + "]";
	}
}