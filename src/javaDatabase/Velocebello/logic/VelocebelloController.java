package javaDatabase.Velocebello.logic;

import javaDatabase.Velocebello.data.*;
import javaDatabase.Velocebello.database.MySQLDatabase;

public class VelocebelloController {

	private MySQLDatabase db;
	
	public VelocebelloController(){
		this.db = new MySQLDatabase();
		db.setup();
	}
	
	/**
	 * trägt einen neuen Kunden in die Datenbank ein
	 * @param k - das neue Kundenobjekt
	 * @return true - wenn erfolgreich
	 */
	public boolean neuerKunde(Kunde k) {
		//hier ggf. Prüfung, ob der Kunde schon vorhanden ist
		return db.neuerKunde(k);
	}

	/**
	 * ermittelt die höchste vorkommende Kundennummer, die in der Datenbank
	 * vorkommt und addiert eins dazu
	 * @return MAX(Kundennummer) + 1
	 */
	public int ermittleNaechsteKundennummer(){
		return db.maxKundennummer() + 1;
	}
	
	/**
	 * holt die Kundenliste aus der Datenbank
	 * @return die Kundenliste
	 */
	public KundenListe getKundenliste(){
		return db.getKundenliste();
	}
}
