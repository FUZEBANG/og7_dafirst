package Kartenspiel.test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.*;

import Kartenspiel.karte.*;

/**
  *
  * GUI-Klasse zum testweisen Anzeigen einer Karte
  *
  * @version 1.0 from 19.10.2012
  * @author Tenbusch
  */

public class KarteGUI extends JFrame {
  // Anfang Attribute
  Held h = new Held("Kitty", "D�mon", "Mistvieh", 25, 75, 50, 50);
  Karte k = new Karte(h);
    private javax.swing.JPanel jPanel;
    private javax.swing.JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;
    // Ende Attribute

  public KarteGUI (String title) {
    super (title);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 500;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    Container cp = getContentPane();
    cp.setLayout(new BorderLayout());
    cp.add(k, BorderLayout.CENTER);
    // Anfang Komponenten
    // Ende Komponenten
    setResizable(true);
    setVisible(true);
  }

  // Anfang Methoden
  // Ende Methoden

  public static void main(String[] args) {
    new KarteGUI("KarteGUI");
  }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
