package Kartenspiel.karte;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class Karte extends JPanel{

    private Held held;
    private JPanel contentPane;
    private JPanel jPanel;
    private JPanel jPanel1;
    private JPanel jPanel2;
    private JPanel jPanel3;

    public Karte() {
    }

    public Karte(Held held) {
        this.setHeld(held);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    }

    public Held getHeld() {
        return held;
    }

    public void setHeld(Held held) {
        this.held = held;
    }
}
