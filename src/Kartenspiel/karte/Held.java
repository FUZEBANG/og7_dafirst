package Kartenspiel.karte;

public class Held {

    private String name;
    private String typ;
    private String beschreibung;
    private int maxLeben;
    private int aktLeben;
    private int angriff;
    private int ruestung;
    private int magieRes;
    private String bildpfad;


    public Held() {
    }

    public Held(String name, String typ, String beschreibung, int maxLeben, int angriff, int ruestung, int magieRes) {
        this.setName(name);
        this.setTyp(typ);
        this.setBeschreibung(beschreibung);
        this.setMaxLeben(maxLeben);
        this.setAktLeben(maxLeben);
        this.setAngriff(angriff);
        this.setRuestung(ruestung);
        this.setMagieRes(magieRes);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public int getMaxLeben() {
        return maxLeben;
    }

    public void setMaxLeben(int maxLeben) {
        this.maxLeben = maxLeben;
    }

    public int getAktLeben() {
        return aktLeben;
    }

    public void setAktLeben(int aktLeben) {
        this.aktLeben = aktLeben;
    }

    public int getAngriff() {
        return angriff;
    }

    public void setAngriff(int angriff) {
        this.angriff = angriff;
    }

    public int getRuestung() {
        return ruestung;
    }

    public void setRuestung(int ruestung) {
        this.ruestung = ruestung;
    }

    public int getMagieRes() {
        return magieRes;
    }

    public void setMagieRes(int magieRes) {
        this.magieRes = magieRes;
    }

    public String getBildpfad() {
        return bildpfad;
    }

    public void setBildpfad(String bildpfad) {
        this.bildpfad = bildpfad;
    }

    public int angreifen() {
        return this.angriff;
    }

    public void heilen() {
        this.aktLeben = this.maxLeben;
    }

    public void leiden(int schaden) {
        if (schaden - this.ruestung <= 0)
            aktLeben -= 1;
    }
}
