package Sortierer;

import java.util.Arrays;
import java.util.Random;

public class Bubblesort {
	
	static int[] list = new int[1000];
	static Random rand = new Random();
	static Stoppuhr uhr = new Stoppuhr();
	
	public static int fuellen() {
		for (int i = 0; i < list.length; i++) {
			int n = rand.nextInt(1001) + 1;
			list[i] = n;
		}
		return 0;
	}
	
	public static void bubblesort(int[] list) {
		int n = list.length;
		int s;
		do {
			int b = 1;
			for (int i = 0; i < n - 1; i++) {
				if (list[i] > list[i + 1]) {
					s = list[i];
					list[i] = list[i + 1];
					list[i + 1] = s;
					b = i + 1;
				}
			}
			n = b;
		} while (n > 1);
		
	}
	
	
	public static void main(String[] args) {
		fuellen();
		for (int p = 0; p < 20; p++) {
			uhr.starte();
			bubblesort(list);
			uhr.stoppe();
			System.out.println(p + ": Die Zeit fuer das Sortieren war: " + uhr.liesMillis() + " Millisekunden");
			System.out.println(p + ": Die Zeit fuer das Sortieren war: " + uhr.liesNanos() + " Nanosekunden");
			uhr.reset();
		}
	}
	

}
